#!/bin/bash

set -e

SESSION_DB_USER=${SESSION_DB_USER:-sessions}
SESSION_DB_PASSWORD=${SESSION_DB_PASSWORD:-password}
SESSION_DB_DATABASE=${SESSION_DB_DATABASE:-sessions}

psql --username $POSTGRES_USER \
  --set=username=$SESSION_DB_USER \
  --set=userpassword=$SESSION_DB_PASSWORD \
  --set=databasename=$SESSION_DB_DATABASE \
  -f ~/sql/01_create.sql

psql --username $SESSION_DB_USER $SESSION_DB_DATABASE \
  -f ~/sql/02_sessions.sql
