CREATE TABLE sessions (
  id SERIAL PRIMARY KEY,
  session_id varchar(100) NOT NULL,
  user_id varchar(100),
  updated_at timestamptz NOT NULL DEFAULT now()
);
