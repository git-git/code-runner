import { Router } from 'express';

import db from '../query/database/database';
import fs from 'fs';
import os from 'os';
import path from 'path';
import uuid from 'uuid/v4';
import { execSync } from 'child_process';

import { getIdFromGlobalId } from '../query/graphql/utilities';
import { getUserProjectById } from '../query/database/db-project';

const router = Router();

const checkParam = async (req, res, next) => {
  const id = getIdFromGlobalId(req.params.globalId, 'Project');
  if (!id) {
    res.send(JSON.stringify({ error: 'wrongId' }));
  }
  const project = await getUserProjectById({
    id,
    userId: req.sessionInfo.userId,
  });
  if (!project) {
    res.send(JSON.stringify({ error: 'wrongId' }));
  }
  req.project = project;
  next();
};

const copyContent = async (folder_id, folder) => {
  const folders = await db('folder')
    .where('parent_id', '=', folder_id)
    .select('id', 'name');
  await Promise.all(
    folders.map(async ({ id, name }) => {
      const newPath = path.join(folder, name);
      fs.mkdirSync(newPath);
      await copyContent(id, newPath);
    }),
  );

  const files = await db('file')
    .where('parent_id', '=', folder_id)
    .select('name', 'content');

  await Promise.all(
    files.map(async ({ name, content }) => {
      const newPath = path.join(folder, name);
      fs.writeFileSync(newPath, content);
    }),
  );
};

const run = async (req, res, next) => {
  const tmpDir = path.join(os.tmpdir(), 'code-runner');
  if (!fs.existsSync(tmpDir)) {
    fs.mkdirSync(tmpDir);
  }

  const dir = path.join(tmpDir, uuid());
  fs.mkdirSync(dir);

  const rootFolder = path.join(dir, req.project.name);
  fs.mkdirSync(rootFolder);
  await copyContent(req.project.root_folder_id, rootFolder);

  fs.writeFileSync(
    path.join(dir, 'Dockerfile'),
    `FROM ubuntu:latest
COPY "./${req.project.name}" /app
WORKDIR /app
RUN chmod +x ./index.sh
CMD ./index.sh
`,
  );

  const index_file = path.join(rootFolder, 'index.sh');
  if (!fs.existsSync(index_file)) {
    fs.writeFileSync(index_file, "echo 'Не найден файл index.sh'");
  }

  const image_id = uuid();
  let answer = '';
  try {
    execSync(`cd "${dir}" && docker build -t ${image_id} .`);
    execSync(`rm -rf ${dir}`);
    answer = execSync(`docker run ${image_id}`).toString();
  } catch (err) {
    answer =
      'Ошибка запуска\n' +
      '\nПоток вывода:\n' +
      err.stdout.toString() +
      '\nПоток вывода ошибок:\n' +
      err.stderr.toString();
  }
  res.send(answer);
};

router.use('/run/:globalId', checkParam);
router.use('/run/:globalId', run);

export default router;
