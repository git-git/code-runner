import { Router } from 'express';

import db from '../query/database/database';
import fs from 'fs';
import os from 'os';
import path from 'path';
import uuid from 'uuid/v4';
import { execSync } from 'child_process';
import fileUpload from 'express-fileupload';

import { getIdFromGlobalId } from '../query/graphql/utilities';
import { getUserProjectById } from '../query/database/db-project';

const router = Router();

const copyContent = async (folder_id, folder, user_id) => {
  await Promise.all(
    fs.readdirSync(folder).map(async name => {
      const curPath = path.join(folder, name);
      if (fs.lstatSync(curPath).isDirectory()) {
        const fId = (await db('folder')
          .returning('id')
          .insert({ name, user_id, parent_id: folder_id }))[0];
        await copyContent(fId, curPath, user_id);
      } else {
        try {
          const content = fs.readFileSync(curPath, { encoding: 'utf8' });
          await db('file')
            .returning('id')
            .insert({ name, user_id, content, parent_id: folder_id });
        } catch (ex) {}
      }
    }),
  );
};

const importArchive = async (req, res, next) => {
  console.log(req.files);
  if (!req.files || !req.files.file || !/^.*\.zip$/.test(req.files.file.name)) {
    return res.send(JSON.stringify({ error: 'No zip file was uploaded.' }));
  }

  const tmpDir = path.join(os.tmpdir(), 'code-runner');
  if (!fs.existsSync(tmpDir)) {
    fs.mkdirSync(tmpDir);
  }

  const dir = path.join(tmpDir, uuid());
  fs.mkdirSync(dir);

  const archivePath = path.join(dir, req.files.file.name);

  await new Promise((resolve, reject) => {
    req.files.file.mv(archivePath, err => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });

  const name = req.files.file.name.substring(0, req.files.file.name.length - 4);

  const rootFolder = path.join(dir, name);
  fs.mkdirSync(rootFolder);

  execSync(`cd ${dir} && unzip "${req.files.file.name}" -d "${rootFolder}"`);

  const user_id = req.sessionInfo.userId;
  if (!user_id) {
    res.send(JSON.stringify({ error: 'notSingedIn' }));
  }

  const root_folder_id = (await db('folder')
    .returning('id')
    .insert({ user_id }))[0];

  const projectId = (await db('project')
    .returning('id')
    .insert({ name, user_id, root_folder_id }))[0];

  await copyContent(root_folder_id, rootFolder, user_id);

  execSync(`rm -rf ${dir}`);

  res.send();
};

router.use('/import', fileUpload());
router.post('/import', importArchive);

export default router;
