import { Router } from 'express';

import db from '../query/database/database';
import fs from 'fs';
import os from 'os';
import path from 'path';
import uuid from 'uuid/v4';
import { execSync } from 'child_process';

import { getIdFromGlobalId } from '../query/graphql/utilities';
import { getUserProjectById } from '../query/database/db-project';

const router = Router();

const checkParam = async (req, res, next) => {
  const id = getIdFromGlobalId(req.params.globalId, 'Project');
  if (!id) {
    res.send(JSON.stringify({ error: 'wrongId' }));
  }
  const project = await getUserProjectById({
    id,
    userId: req.sessionInfo.userId,
  });
  if (!project) {
    res.send(JSON.stringify({ error: 'wrongId' }));
  }
  req.project = project;
  next();
};

const copyContent = async (folder_id, folder) => {
  const folders = await db('folder')
    .where('parent_id', '=', folder_id)
    .select('id', 'name');
  await Promise.all(
    folders.map(async ({ id, name }) => {
      const newPath = path.join(folder, name);
      fs.mkdirSync(newPath);
      await copyContent(id, newPath);
    }),
  );

  const files = await db('file')
    .where('parent_id', '=', folder_id)
    .select('name', 'content');

  await Promise.all(
    files.map(async ({ name, content }) => {
      const newPath = path.join(folder, name);
      fs.writeFileSync(newPath, content);
    }),
  );
};

const createArcive = async (req, res, next) => {
  const tmpDir = path.join(os.tmpdir(), 'code-runner');
  if (!fs.existsSync(tmpDir)) {
    fs.mkdirSync(tmpDir);
  }

  const dir = path.join(tmpDir, uuid());
  fs.mkdirSync(dir);

  const rootFolder = path.join(dir, req.project.name);
  fs.mkdirSync(rootFolder);

  await copyContent(req.project.root_folder_id, rootFolder);

  execSync(
    `cd "${dir}" && zip -r "${req.project.name}.zip" "${req.project.name}"`,
  );

  res.sendFile(rootFolder + '.zip');
  res.on('finish', () => {
    execSync(`rm -rf ${dir}`);
  });
};

router.use('/export/:globalId', checkParam);
router.use('/export/:globalId', createArcive);

export default router;
