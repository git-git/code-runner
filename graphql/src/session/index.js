import { Router } from 'express';
import cookieSession from 'cookie-session';
import uuid from 'uuid/v4';

import { getUserId, setUserId, cleanUpSessionId } from './db-sessions';

const router = Router();

router.use(
  cookieSession({
    name: 'session',
    secret: 'my-secret',
    maxAge: 24 * 60 * 60 * 1000,
  }),
);

router.use(async (req, res, next) => {
  let sessionId = req.session.id || uuid();
  req.session = { id: sessionId };

  req.sessionInfo = {
    userId: await getUserId(sessionId),
    setUserId: async userId => {
      if (userId) {
        await setUserId(sessionId, userId);
        req.sessionInfo.userId = userId;
      } else {
        await cleanUpSessionId(sessionId);
        req.sessionInfo.userId = null;
      }
    },
  };

  next();
});

export default router;
