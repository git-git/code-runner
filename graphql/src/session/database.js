import knex from 'knex';

const config = {
  development: {
    client: 'pg',
    connection: {
      host: 'localhost',
      port: 12005,
      user: 'sessions',
      password: 'password',
      database: 'sessions',
    },
  },
  production: {
    client: 'pg',
    connection: {
      host: process.env.SESSION_DB_HOST,
      port: process.env.SESSION_DB_PORT,
      user: process.env.SESSION_DB_USER,
      password: process.env.SESSION_DB_PASSWORD,
      database: process.env.SESSION_DB_DATABASE,
    },
  },
};

const current_config =
  process.env.NODE_ENV === 'production'
    ? config.production
    : config.development;

const db = knex(current_config);

export default db;
