import db from './database';

const table = () => db('user');
const rawByField = (fieldName, value) => table().where(fieldName, '=', value);

const dataExists = data => data && data[0];

export const getUserById = async id => {
  const fields = ['login', 'name', 'password'];
  const data = await rawByField('id', id).select(...fields);
  return dataExists(data) ? { id, ...data[0] } : null;
};

export const getUserByLogin = async login => {
  const fields = ['id', 'name', 'password'];
  const data = await rawByField('login', login).select(...fields);
  return dataExists(data) ? { login, ...data[0] } : null;
};

export const changePassword = async ({ id, password }) => {
  await rawByField('id', id).update({ password, updated_at: new Date() });
};

export const changeName = async ({ id, name }) => {
  await rawByField('id', id).update({ name, updated_at: new Date() });
};

export const addUser = async ({ login, name, password }) => {
  const timestamp = { created_at: new Date(), updated_at: new Date() };
  const data = { login, name, password, ...timestamp };

  const returningData = await table()
    .returning('id')
    .insert(data);

  return dataExists(returningData) ? returningData[0] : null;
};
