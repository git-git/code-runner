import db from './database';

import { checkIfFolderNameInUse } from './db-folder';

const table = () => db('file');
const userFile = userId => table().where('user_id', '=', userId);
const userFileById = (userId, id) => userFile(userId).where('id', '=', id);

const dataExists = data => data && data[0];

export const getUserFileById = async ({ id, userId }) => {
  const data = await userFileById(userId, id).select('name', 'content');
  return dataExists(data) ? { id, ...data[0] } : null;
};

export const getFielsIdsList = async ({ folderId, userId }) => {
  return await userFile(userId)
    .where('parent_id', '=', folderId)
    .select('id');
};

export const checkIfFileNameInUse = async ({ folderId, name, userId }) => {
  const data = await userFile(userId)
    .where('parent_id', '=', folderId)
    .where('name', '=', name)
    .select();
  return data.length !== 0;
};

export const addFile = async ({ folderId, name, userId, content }) => {
  if (
    (await checkIfFolderNameInUse({ folderId, name, userId })) ||
    (await checkIfFileNameInUse({ folderId, name, userId }))
  ) {
    return 'NameAlreadyInUse';
  }

  const timestamp = { created_at: new Date(), updated_at: new Date() };
  const data = {
    name,
    user_id: userId,
    parent_id: folderId,
    content,
    ...timestamp,
  };
  const returningData = await table()
    .returning('id')
    .insert(data);
  return dataExists(returningData) ? returningData[0] : null;
};

export const removeFileById = async ({ id, userId }) => {
  const count = await userFileById(userId, id).delete();
  return { error: 'wrongId' };
};

export const changeFileName = async ({ id, name, userId }) => {
  const retData = await userFileById(userId, id).select('parent_id');
  const file = dataExists(retData) ? retData[0] : null;

  if (!file) {
    return 'wrongId';
  }

  const folderId = file.parent_id;
  if (
    (await checkIfFolderNameInUse({ folderId, name, userId })) ||
    (await checkIfFileNameInUse({ folderId, name, userId }))
  ) {
    return 'nameAlreadyInUse';
  }

  await userFileById(userId, id).update({ name, updated_at: new Date() });
};

export const moveFile = async ({ id, folderId, userId }) => {
  const retData = await userFileById(userId, id).select('name');
  const file = dataExists(retData) ? retData[0] : null;

  if (!file) {
    return 'wrongId';
  }

  if (
    (await checkIfFolderNameInUse({ folderId, name: file.name, userId })) ||
    (await checkIfFileNameInUse({ folderId, name: file.name, userId }))
  ) {
    return 'nameAlreadyInUse';
  }

  const data = { parent_id: folderId, updated_at: new Date() };
  await userFileById(userId, id).update(data);
};

export const changeFileContent = async ({ id, content, userId }) => {
  const count = await userFileById(userId, id).update({
    content,
    updated_at: new Date(),
  });
  if (count == 0) return 'wrongId';
};
