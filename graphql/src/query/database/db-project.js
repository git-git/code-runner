import db from './database';

import { removeFolderById } from './db-folder';

const table = () => db('project');
const userProjects = userId => table().where('user_id', '=', userId);
const userProjectsById = (userId, id) =>
  userProjects(userId).where('id', '=', id);

const foldersTable = () => db('folder');

const dataExists = data => data && data[0];

export const getUserProjectIdsList = async ({ userId }) => {
  return await userProjects(userId).select('id');
};

export const getUserProjectById = async ({ id, userId }) => {
  const fields = ['name', 'root_folder_id'];
  const data = await userProjectsById(userId, id).select(...fields);
  return dataExists(data) ? { id, ...data[0] } : null;
};

export const addUserProject = async ({ name, userId }) => {
  const timestamp = { created_at: new Date(), updated_at: new Date() };

  const rootFolderData = { name: null, user_id: userId, ...timestamp };
  const rootFolderReturningData = await foldersTable()
    .returning('id')
    .insert(rootFolderData);
  const root_folder_id = dataExists(rootFolderReturningData)
    ? rootFolderReturningData[0]
    : null;

  if (!root_folder_id) {
    return null;
  }

  const data = { name, user_id: userId, root_folder_id, ...timestamp };
  const returningData = await table()
    .returning('id')
    .insert(data);

  return dataExists(returningData) ? returningData[0] : null;
};

export const removeUserProjectById = async ({ id, userId }) => {
  const data = await getUserProjectById({ id, userId });
  if (!data) return 'wrongId';
  await userProjectsById(userId, id).delete();
  await removeFolderById({ id: data.root_folder_id, userId });
};

export const changeUserProjectName = async ({ id, name, userId }) => {
  const count = await userProjectsById(userId, id).update({
    name,
    updated_at: new Date(),
  });
  if (count == 0) return 'wrongId';
};
