import db from './database';

import {
  getFielsIdsList,
  removeFileById,
  checkIfFileNameInUse,
} from './db-file';

const table = () => db('folder');
const userFolders = userId => table().where('user_id', '=', userId);
const userFoldersById = (userId, id) =>
  userFolders(userId).where('id', '=', id);

const dataExists = data => data && data[0];

export const getSubFoldersIdsList = async ({ folderId, userId }) => {
  return await userFolders(userId)
    .where('parent_id', '=', folderId)
    .select('id');
};

export const getUserFolderById = async ({ id, userId }) => {
  const data = await userFoldersById(userId, id).select('name', 'parent_id');
  return dataExists(data) ? { id, ...data[0] } : null;
};

export const checkIfFolderNameInUse = async ({ folderId, name, userId }) => {
  const data = await userFolders(userId)
    .where('parent_id', '=', folderId)
    .where('name', '=', name)
    .select();
  return data.length !== 0;
};

export const addSubFolder = async ({ folderId, name, userId }) => {
  if (
    (await checkIfFolderNameInUse({ folderId, name, userId })) ||
    (await checkIfFileNameInUse({ folderId, name, userId }))
  ) {
    return 'nameAlreadyInUse';
  }

  const timestamp = { created_at: new Date(), updated_at: new Date() };
  const data = { name, user_id: userId, parent_id: folderId, ...timestamp };
  const returningData = await table()
    .returning('id')
    .insert(data);
  return dataExists(returningData) ? returningData[0] : null;
};

export const removeFolderById = async ({ id, userId }) => {
  const subFoldersList = await getSubFoldersIdsList({ folderId: id, userId });
  await Promise.all(
    subFoldersList.map(({ id: subId }) =>
      removeFolderById({ id: subId, userId }),
    ),
  );

  const fielsList = await getFielsIdsList({ folderId: id, userId });
  await Promise.all(fielsList.map(({ id }) => removeFileById({ id, userId })));

  const count = await userFoldersById(userId, id).delete();
  return { error: 'wrongId' };
};

export const changeFolderName = async ({ id, name, userId }) => {
  const retData = await userFoldersById(userId, id).select('parent_id');
  const folder = dataExists(retData) ? retData[0] : null;

  if (!folder) {
    return 'wrongId';
  }

  const folderId = folder.parent_id;
  if (
    (await checkIfFolderNameInUse({ folderId, name, userId })) ||
    (await checkIfFileNameInUse({ folderId, name, userId }))
  ) {
    return 'nameAlreadyInUse';
  }

  await userFoldersById(userId, id).update({ name, updated_at: new Date() });
};

export const moveFolder = async ({ id, rootFolderId, userId }) => {
  const retData = await userFoldersById(userId, id).select('name');
  const folder = dataExists(retData) ? retData[0] : null;

  if (!folder) {
    return 'wrongId';
  }

  if (
    (await checkIfFolderNameInUse({
      folderId: rootFolderId,
      name: folder.name,
      userId,
    })) ||
    (await checkIfFileNameInUse({
      folderId: rootFolderId,
      name: folder.name,
      userId,
    }))
  ) {
    return 'nameAlreadyInUse';
  }

  const checkParent = async ({ id, parent_id }) => {
    const data = await userFoldersById(userId, id).select('parent_id');
    const folder = dataExists(data) ? data[0] : null;
    if (folder.parent_id == parent_id) {
      return true;
    } else if (!folder.parent_id) {
      return false;
    }
    return await checkParent({ id: folder.parent_id, parent_id });
  };

  if (id === rootFolderId) return 'circuleStructure';
  if (await checkParent({ id: rootFolderId, parent_id: id }))
    return 'circuleStructure';
  const data = { parent_id: rootFolderId, updated_at: new Date() };
  await userFoldersById(userId, id).update(data);
};
