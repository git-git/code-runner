import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInputObjectType,
} from 'graphql';

import {
  getUserProjectIdsList,
  getUserProjectById,
  addUserProject,
  removeUserProjectById,
  changeUserProjectName,
} from '../database/db-project';

import { nodeInterface } from './node';

import {
  createMutation,
  createPayload,
  createConnection,
  getIdFromGlobalId,
} from './utilities';

import { fromGlobalId, toGlobalId, globalIdField } from 'graphql-relay';

import { FolderField } from './folder';

// === === === === === === QUERY === === === === === ===

export const ProjectType = new GraphQLObjectType({
  name: 'Project',
  interfaces: [nodeInterface],
  fields: () => ({
    id: globalIdField(),
    name: {
      name: 'Name',
      type: GraphQLString,
    },
    rootFolder: FolderField,
  }),
});

export const ProjectField = {
  type: ProjectType,
  resolve: async (obj, args, context, root) => {
    const userId = context.userId;
    const data = await getUserProjectById({ id: root.id, userId });

    return { ...data, rootFolder: { id: data.root_folder_id } };
  },
};

export const ProjectConnection = createConnection({
  field: ProjectField,
  resolveIdsList: async (obj, args, context) => {
    const userId = context.userId;
    return await getUserProjectIdsList({ userId });
  },
});

// === === === === === === MUTATIONS === === === === === ===

const projectMutationPayload = createPayload({
  name: 'ProjectMutationPayload',
  fields: () => ({
    project: { name: 'Project', type: ProjectType },
    error: { name: 'Error', type: GraphQLString },
  }),
});

// === === === === === === ADD PROJECT === === === === === ===

const addInput = {
  name: 'ProjectAddInput',
  fields: () => ({
    name: { name: 'Name', type: GraphQLString },
  }),
};

const addResolve = async (obj, args, context) => {
  const userId = context.userId;
  const { name } = args;
  if (!userId) {
    return { error: 'notSignedIn' };
  }

  const id = await addUserProject({ name, userId });
  const project = await getUserProjectById({ id, userId });

  return { project };
};

export const ProjectAddField = createMutation({
  input: addInput,
  payload: projectMutationPayload,
  resolve: addResolve,
});

// === === === === === === REMOVE PROJECT === === === === === ===

const removeInput = {
  name: 'ProjectRemoveInput',
  fields: () => ({
    id: { name: 'Project Id', type: GraphQLString },
  }),
};

const removeResolve = async (obj, args, context) => {
  const userId = context.userId;
  const { id: globalId } = args;
  if (!userId) {
    return { error: 'notSignedIn' };
  }

  const id = getIdFromGlobalId(globalId, 'Project');
  if (!id) {
    return { error: 'wrongId' };
  }

  const ans = await removeUserProjectById({ id, userId });
  if (ans == 'wrongId') {
    return { error: 'wrongId' };
  }

  return {};
};

export const ProjectRemoveField = createMutation({
  input: removeInput,
  payload: projectMutationPayload,
  resolve: removeResolve,
});

// === === === === === === CHANGE NAME === === === === === ===

const changeNameInput = {
  name: 'ProjectChangeNameInput',
  fields: () => ({
    id: { name: 'Project Id', type: GraphQLString },
    name: { name: 'Name', type: GraphQLString },
  }),
};

const changeNameResolve = async (obj, args, context) => {
  const userId = context.userId;
  const { id: globalId, name } = args;
  if (!userId) {
    return { error: 'notSignedIn' };
  }

  const id = getIdFromGlobalId(globalId, 'Project');
  if (!id) {
    return { error: 'wrongId' };
  }

  const ans = await changeUserProjectName({ id, name, userId });
  if (ans == 'wrongId') {
    return { error: 'wrongId' };
  }
  const project = await getUserProjectById({ id, userId });

  return { project };
};

export const ProjectChangeNameField = createMutation({
  input: changeNameInput,
  payload: projectMutationPayload,
  resolve: changeNameResolve,
});
