import { nodeDefinitions, fromGlobalId } from 'graphql-relay';

import { getUserById } from '../database/db-user';
import { getUserProjectById } from '../database/db-project';
import { getUserFolderById } from '../database/db-folder';
import { getUserFileById } from '../database/db-file';

const { nodeInterface, nodeField } = nodeDefinitions(
  async (globalId, context) => {
    const userId = await context.userId;
    const { type, id } = fromGlobalId(globalId);

    switch (type) {
      case 'User':
        if (id !== userId) return null;
        return {
          ...(await getUserById(id)),
          _type: require('./user').UserType,
        };
      case 'Project':
        const Project = await getUserProjectById({ id, userId });
        if (!Project) return null;
        return { ...Project, _type: require('./project').ProjectType };
      case 'Folder':
        const Folder = await getUserFolderById({ id, userId });
        if (!Folder) return null;
        return { ...Folder, _type: require('./folder').FolderType };
      case 'File':
        const File = await getUserFileById({ id, userId });
        if (!File) return null;
        return { ...File, _type: require('./file').FileType };
      default:
        return null;
    }
  },
  obj => obj && obj._type,
);

export { nodeInterface, nodeField };
