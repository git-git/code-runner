import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInputObjectType,
} from 'graphql';

import {
  getUserFileById,
  getFielsIdsList,
  addFile,
  removeFileById,
  changeFileName,
  moveFile,
  changeFileContent,
} from '../database/db-file';

import { getUserFolderById } from '../database/db-folder';

import { nodeInterface } from './node';

import {
  createMutation,
  createPayload,
  createConnection,
  getIdFromGlobalId,
} from './utilities';

import { fromGlobalId, toGlobalId, globalIdField } from 'graphql-relay';

// === === === === === === QUERY === === === === === ===

export const FileType = new GraphQLObjectType({
  name: 'File',
  interfaces: [nodeInterface],
  fields: () => ({
    id: globalIdField(),
    name: {
      name: 'Name',
      type: GraphQLString,
    },
    content: {
      name: 'Content',
      type: GraphQLString,
    },
  }),
});

export const FileField = {
  type: FileType,
  resolve: async (obj, args, context, root) => {
    const id = root.id;
    const userId = context.userId;
    return await getUserFileById({ id, userId });
  },
};

export const FileConnection = createConnection({
  field: FileField,
  resolveIdsList: async (obj, args, context) => {
    const folderId = obj.id;
    const userId = context.userId;
    return await getFielsIdsList({ folderId, userId });
  },
});

// === === === === === === MUTATIONS === === === === === ===

const fileMutationPayload = createPayload({
  name: 'FileMutationPayload',
  fields: () => ({
    file: { name: 'File', type: FileType },
    error: { name: 'Error', type: GraphQLString },
  }),
});

// === === === === === === ADD FILE === === === === === ===

const addInput = {
  name: 'FileAddInput',
  fields: () => ({
    folderId: { name: 'Folder Id', type: GraphQLString },
    name: { name: 'Name', type: GraphQLString },
    content: { name: 'Content', type: GraphQLString },
  }),
};

const addResolve = async (obj, args, context) => {
  const userId = context.userId;
  const { name, folderId: folderGlobalId, content } = args;
  if (!userId) {
    return { error: 'notSignedIn' };
  }

  const folderId = getIdFromGlobalId(folderGlobalId, 'Folder');
  if (!folderId) {
    return { error: 'wrongFolderId' };
  }
  const folderData = await getUserFolderById({ id: folderId, userId });
  if (!folderData) {
    return { error: 'wrongMainFolderId' };
  }

  const id = await addFile({ folderId: folderData.id, name, content, userId });

  if (id === 'NameAlreadyInUse') {
    return { error: 'NameAlreadyInUse' };
  }

  const file = await getUserFileById({ id, userId });

  return { file };
};

export const FileAddField = createMutation({
  input: addInput,
  payload: fileMutationPayload,
  resolve: addResolve,
});

// === === === === === === REMOVE FILE === === === === === ===

const removeInput = {
  name: 'FileRemoveInput',
  fields: () => ({
    id: { name: 'File Id', type: GraphQLString },
  }),
};

const removeResolve = async (obj, args, context) => {
  const userId = context.userId;
  const { id: globalId } = args;
  if (!userId) {
    return { error: 'notSignedIn' };
  }

  const id = getIdFromGlobalId(globalId, 'File');
  if (!id) {
    return { error: 'wrongId' };
  }

  const data = await getUserFileById({ id, userId });
  if (!data) {
    return { error: 'wrongId' };
  }

  await removeFileById({ id, userId });

  return {};
};

export const FileRemoveField = createMutation({
  input: removeInput,
  payload: fileMutationPayload,
  resolve: removeResolve,
});

// === === === === === === CHANGE NAME === === === === === ===

const changeNameInput = {
  name: 'FileChangeNameInput',
  fields: () => ({
    id: { name: 'File Id', type: GraphQLString },
    name: { name: 'Name', type: GraphQLString },
  }),
};

const changeNameResolve = async (obj, args, context) => {
  const userId = context.userId;
  const { id: globalId, name } = args;
  if (!userId) {
    return { error: 'notSignedIn' };
  }

  const id = getIdFromGlobalId(globalId, 'File');
  if (!id) {
    return { error: 'wrongId' };
  }

  const data = await getUserFileById({ id, userId });
  if (!data) {
    return { error: 'wrongId' };
  }

  const ans = await changeFileName({ id, name, userId });
  if (ans === 'wrongId') return { error: 'wrongId' };
  if (ans === 'nameAlreadyInUse') return { error: 'nameAlreadyInUse' };
  const file = await getUserFileById({ id, name, userId });

  return { file };
};

export const FileChangeNameField = createMutation({
  input: changeNameInput,
  payload: fileMutationPayload,
  resolve: changeNameResolve,
});

// === === === === === === CHANGE CONTENT === === === === === ===

const changeContentInput = {
  name: 'FileChangeContentInput',
  fields: () => ({
    id: { name: 'File Id', type: GraphQLString },
    content: { name: 'Content', type: GraphQLString },
  }),
};

const changeContentResolve = async (obj, args, context) => {
  const userId = context.userId;
  const { id: globalId, content } = args;
  if (!userId) {
    return { error: 'notSignedIn' };
  }

  const id = getIdFromGlobalId(globalId, 'File');
  if (!id) {
    return { error: 'wrongId' };
  }

  const data = await getUserFileById({ id, userId });
  if (!data) {
    return { error: 'wrongId' };
  }

  const ans = await changeFileContent({ id, content, userId });
  if (ans === 'wrongId') return { error: 'wrongId' };
  const file = await getUserFileById({ id, userId });

  return { file };
};

export const FileChangeContentField = createMutation({
  input: changeContentInput,
  payload: fileMutationPayload,
  resolve: changeContentResolve,
});

// === === === === === === MOVE FILE === === === === === ===

const moveInput = {
  name: 'FileMoveInput',
  fields: () => ({
    id: { name: 'Project Id', type: GraphQLString },
    folderId: { name: 'New Folder Id', type: GraphQLString },
  }),
};

const moveResolve = async (obj, args, context) => {
  const userId = context.userId;
  const { id: globalId, folderId: globalFolderId } = args;
  if (!userId) {
    return { error: 'notSignedIn' };
  }

  const id = getIdFromGlobalId(globalId, 'File');
  const baseFile = await getUserFileById({ id, userId });
  if (!baseFile) {
    return { error: 'wrongId' };
  }

  const folderId = getIdFromGlobalId(globalFolderId, 'Folder');
  const folder = await getUserFolderById({ id: folderId, userId });
  if (!folder) {
    return { error: 'wrongFolderId' };
  }

  const ans = await moveFile({ id, folderId, userId });
  if (ans === 'wrongId') return { error: 'wrongId' };
  if (ans === 'nameAlreadyInUse') return { error: 'nameAlreadyInUse' };
  const file = await getUserFileById({ id, userId });
  return { file };
};

export const FileMoveField = createMutation({
  input: moveInput,
  payload: fileMutationPayload,
  resolve: moveResolve,
});
