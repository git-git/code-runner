import { nodeField } from './node';

import {
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
  GraphQLInputObjectType,
} from 'graphql';

import {
  UserField,
  UserSignInField,
  UserSignUpField,
  UserSignOutField,
  UserChangePasswordField,
  UserChangeNameField,
} from './user';

import {
  ProjectAddField,
  ProjectRemoveField,
  ProjectChangeNameField,
} from './project';

import {
  FolderAddField,
  FolderRemoveField,
  FolderChangeNameField,
  FolderMoveField,
} from './folder';

import {
  FileAddField,
  FileRemoveField,
  FileChangeNameField,
  FileChangeContentField,
  FileMoveField,
} from './file';

const query = new GraphQLObjectType({
  description: 'Main query',
  name: 'Query',
  fields: () => ({
    user: UserField,
    node: nodeField,
  }),
});

const mutation = new GraphQLObjectType({
  description: 'Main mutations',
  name: 'Mutation',
  fields: () => ({
    UserSignIn: UserSignInField,
    UserSignUp: UserSignUpField,
    UserSignOut: UserSignOutField,
    UserChangePassword: UserChangePasswordField,
    UserChangeName: UserChangeNameField,
    ProjectAdd: ProjectAddField,
    ProjectRemove: ProjectRemoveField,
    ProjectChangeName: ProjectChangeNameField,
    FolderAdd: FolderAddField,
    FolderRemove: FolderRemoveField,
    FolderChangeName: FolderChangeNameField,
    FolderMove: FolderMoveField,
    FileAdd: FileAddField,
    FileRemove: FileRemoveField,
    FileChangeName: FileChangeNameField,
    FileChangeContent: FileChangeContentField,
    FileMove: FileMoveField,
  }),
});

export default new GraphQLSchema({
  query,
  mutation,
});
