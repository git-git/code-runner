import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInputObjectType,
} from 'graphql';

import {
  getUserFolderById,
  getSubFoldersIdsList,
  addSubFolder,
  removeFolderById,
  changeFolderName,
  moveFolder,
} from '../database/db-folder';

import { nodeInterface } from './node';

import {
  createMutation,
  createPayload,
  createConnection,
  getIdFromGlobalId,
} from './utilities';

import { fromGlobalId, toGlobalId, globalIdField } from 'graphql-relay';

import { FileConnection } from './file';

// === === === === === === QUERY === === === === === ===

export const FolderType = new GraphQLObjectType({
  name: 'Folder',
  interfaces: [nodeInterface],
  fields: () => ({
    id: globalIdField(),
    name: {
      name: 'Name',
      type: GraphQLString,
    },
    subFolders: FolderConnection,
    files: FileConnection,
  }),
});

export const FolderField = {
  type: FolderType,
  resolve: async (obj, args, context, root) => {
    const id = (root && root.id) || obj.root_folder_id;
    const userId = context.userId;
    return await getUserFolderById({ id, userId });
  },
};

export const FolderConnection = createConnection({
  field: FolderField,
  resolveIdsList: async (obj, args, context) => {
    const folderId = obj.id;
    const userId = context.userId;
    return await getSubFoldersIdsList({ folderId, userId });
  },
});

// === === === === === === MUTATIONS === === === === === ===

const folderMutationPayload = createPayload({
  name: 'FolderMutationPayload',
  fields: () => ({
    folder: { name: 'Folder', type: FolderType },
    error: { name: 'Error', type: GraphQLString },
  }),
});

// === === === === === === ADD FOLDER === === === === === ===

const addInput = {
  name: 'FolderAddInput',
  fields: () => ({
    mainFolderId: { name: 'Main Folder Id', type: GraphQLString },
    name: { name: 'Name', type: GraphQLString },
  }),
};

const addResolve = async (obj, args, context) => {
  const userId = context.userId;
  const { name, mainFolderId: mainFolderGlobalId } = args;
  if (!userId) {
    return { error: 'notSignedIn' };
  }

  const mainFolderId = getIdFromGlobalId(mainFolderGlobalId, 'Folder');
  if (!mainFolderId) {
    return { error: 'wrongMainFolderId' };
  }
  const rootFolderData = await getUserFolderById({ id: mainFolderId, userId });
  if (!rootFolderData) {
    return { error: 'wrongMainFolderId' };
  }

  const ans = await addSubFolder({ folderId: rootFolderData.id, name, userId });

  if (ans === 'nameAlreadyInUse') {
    return { error: 'nameAlreadyInUse' };
  }

  const id = ans;

  const folder = await getUserFolderById({ id, userId });

  return { folder };
};

export const FolderAddField = createMutation({
  input: addInput,
  payload: folderMutationPayload,
  resolve: addResolve,
});

// === === === === === === REMOVE FOLDER === === === === === ===

const removeInput = {
  name: 'FolderRemoveInput',
  fields: () => ({
    id: { name: 'Folder Id', type: GraphQLString },
  }),
};

const removeResolve = async (obj, args, context) => {
  const userId = context.userId;
  const { id: globalId } = args;
  if (!userId) {
    return { error: 'notSignedIn' };
  }

  const id = getIdFromGlobalId(globalId, 'Folder');
  if (!id) {
    return { error: 'wrongId' };
  }

  const data = await getUserFolderById({ id, userId });
  if (!data) {
    return { error: 'wrongId' };
  }
  if (!data.parent_id) {
    return { error: 'cannotRemoveRootFolder' };
  }

  await removeFolderById({ id, userId });

  return {};
};

export const FolderRemoveField = createMutation({
  input: removeInput,
  payload: folderMutationPayload,
  resolve: removeResolve,
});

// === === === === === === CHANGE NAME === === === === === ===

const changeNameInput = {
  name: 'FolderChangeNameInput',
  fields: () => ({
    id: { name: 'Folder Id', type: GraphQLString },
    name: { name: 'Name', type: GraphQLString },
  }),
};

const changeNameResolve = async (obj, args, context) => {
  const userId = context.userId;
  const { id: globalId, name } = args;
  if (!userId) {
    return { error: 'notSignedIn' };
  }

  const id = getIdFromGlobalId(globalId, 'Folder');
  if (!id) {
    return { error: 'wrongId' };
  }

  const data = await getUserFolderById({ id, userId });
  if (!data) {
    return { error: 'wrongId' };
  }
  if (!data.parent_id) {
    return { error: 'cannotChangeRootFolderName' };
  }

  const ans = await changeFolderName({ id, name, userId });
  if (ans === 'wrongId') return { error: 'wrongId' };
  if (ans === 'nameAlreadyInUse') return { error: 'nameAlreadyInUse' };
  const folder = await getUserFolderById({ id, name, userId });

  return { folder };
};

export const FolderChangeNameField = createMutation({
  input: changeNameInput,
  payload: folderMutationPayload,
  resolve: changeNameResolve,
});

// === === === === === === MOVE FOLDER === === === === === ===

const moveInput = {
  name: 'FolderMoveInput',
  fields: () => ({
    id: { name: 'Project Id', type: GraphQLString },
    mainFolderId: { name: 'New Parent Id', type: GraphQLString },
  }),
};

const moveResolve = async (obj, args, context) => {
  const userId = context.userId;
  const { id: globalId, mainFolderId } = args;
  if (!userId) {
    return { error: 'notSignedIn' };
  }

  const id = getIdFromGlobalId(globalId, 'Folder');
  const baseFolder = await getUserFolderById({ id, userId });
  if (!id || !baseFolder) {
    return { error: 'wrongId' };
  }
  if (!baseFolder.parent_id) {
    return { error: 'cannotMoveRootFolder' };
  }

  const rootFolderId = getIdFromGlobalId(mainFolderId, 'Folder');
  if (!rootFolderId || !await getUserFolderById({ id: rootFolderId, userId })) {
    return { error: 'wrongMainFolderId' };
  }

  const ans = await moveFolder({ id, rootFolderId, userId });
  if (ans === 'circuleStructure') return { error: 'circuleStructure' };
  if (ans === 'nameAlreadyInUse') return { error: 'nameAlreadyInUse' };
  if (ans === 'wrongId') return 'wrongId';
  const folder = await getUserFolderById({ id, userId });
  return { folder };
};

export const FolderMoveField = createMutation({
  input: moveInput,
  payload: folderMutationPayload,
  resolve: moveResolve,
});
