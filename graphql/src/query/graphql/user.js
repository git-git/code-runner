import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInputObjectType,
} from 'graphql';

import {
  getUserById,
  getUserByLogin,
  addUser,
  changePassword,
  changeName,
} from '../database/db-user';

import { nodeInterface } from './node';

import { createMutation, createPayload } from './utilities';

import { fromGlobalId, toGlobalId, globalIdField } from 'graphql-relay';

import { ProjectConnection } from './project';

// === === === === === === QUERY === === === === === ===

export const UserType = new GraphQLObjectType({
  description: 'Current user',
  name: 'User',
  interfaces: [nodeInterface],
  fields: () => ({
    id: globalIdField(),
    name: {
      description: 'User name',
      name: 'Name',
      type: GraphQLString,
    },
    login: {
      description: 'User login',
      name: 'Login',
      type: GraphQLString,
    },
    project: ProjectConnection,
  }),
});

export const UserField = {
  type: UserType,
  resolve: async (obj, args, context) => {
    return await getUserById(context.userId);
  },
};

// === === === === === === MUTATIONS === === === === === ===

const userMutationPayload = createPayload({
  name: 'UserMutationPayload',
  fields: () => ({
    user: { name: 'User', type: UserType },
    error: { name: 'Error', type: GraphQLString },
  }),
});

// === === === === === === SIGN IN === === === === === ===

const signInInput = {
  name: 'UserSignInInput',
  fields: () => ({
    login: { name: 'Login', type: GraphQLString },
    password: { name: 'Password', type: GraphQLString },
  }),
};

const signInResolve = async (obj, args, context) => {
  const id = context.userId;
  if (id) {
    return { error: 'alreadySignedIn' };
  }
  const user = await getUserByLogin(args.login);
  if (user && user.password === args.password) {
    await context.setUserId(user.id);
    return { user };
  }
  return { error: 'wrongCredantials' };
};

export const UserSignInField = createMutation({
  input: signInInput,
  payload: userMutationPayload,
  resolve: signInResolve,
});

// === === === === === === SIGN UP === === === === === ===

const signUpInput = {
  name: 'UserSignUpInput',
  fields: () => ({
    name: { name: 'Name', type: GraphQLString },
    login: { name: 'Login', type: GraphQLString },
    password: { name: 'password', type: GraphQLString },
  }),
};

const signUpResolve = async (obj, args, context) => {
  const contextId = context.userId;
  const { login, name, password } = args;

  if (contextId) {
    return { error: 'alreadySignedIn' };
  }

  const signedUpUser = await getUserByLogin(args.login);
  if (signedUpUser) {
    return { error: 'logginAlreadyInUse' };
  }

  const id = await addUser({ login, name, password });
  const user = await getUserById(id);
  await context.setUserId(user.id);

  return { user };
};

export const UserSignUpField = createMutation({
  input: signUpInput,
  payload: userMutationPayload,
  resolve: signUpResolve,
});

// === === === === === === SIGN OUT === === === === === ===

const signOutInput = {
  name: 'UserSignOutInput',
  fields: () => ({}),
};

const signOutResolve = async (obj, args, context) => {
  const id = context.userId;
  if (!id) {
    return { error: 'notSignedIn' };
  }
  await context.setUserId(null);
  return {};
};

export const UserSignOutField = createMutation({
  input: signOutInput,
  payload: userMutationPayload,
  resolve: signOutResolve,
});

// === === === === === === CHANGE PASSWORD === === === === === ===

const changePasswordInput = {
  name: 'UserChangePasswordInput',
  fields: () => ({
    password: { name: 'password', type: GraphQLString },
  }),
};

const changePasswordResolve = async (obj, args, context) => {
  const id = context.userId;
  const { password } = args;

  if (!id) {
    return { error: 'notSignedIn' };
  }

  await changePassword({ id, password });
  const user = await getUserById(id);

  return { user };
};

export const UserChangePasswordField = createMutation({
  input: changePasswordInput,
  payload: userMutationPayload,
  resolve: changePasswordResolve,
});

// === === === === === === CHANGE NAME === === === === === ===

const changeNameInput = {
  name: 'UserChangeNameInput',
  fields: () => ({
    name: { name: 'password', type: GraphQLString },
  }),
};

const changeNameResolve = async (obj, args, context) => {
  const id = context.userId;
  const { name } = args;

  if (!id) {
    return { error: 'notSignedIn' };
  }

  await changeName({ id, name });
  const user = await getUserById(id);

  return { user };
};

export const UserChangeNameField = createMutation({
  input: changeNameInput,
  payload: userMutationPayload,
  resolve: changeNameResolve,
});
