import { Router } from 'express';
import graphqlHTTP from 'express-graphql';
import schema from './graphql/schema';

const router = Router();

const developerMode = true; //process.env.NODE_ENV !== 'production';

router.use(
  graphqlHTTP(req => ({
    schema,
    graphiql: developerMode,
    context: req.sessionInfo,
    formatError: error => ({
      message: error.message,
      locations: error.locations,
      stack: error.stack ? error.stack.split('\n') : [],
      path: error.path,
    }),
  })),
);

export default router;
