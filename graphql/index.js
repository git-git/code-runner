import express from 'express';
import cors from 'cors';

import resolveSession from './src/session';
import resolveQuery from './src/query';
import resolveExport from './src/export';
import resolveImport from './src/import';
import resolveRun from './src/run';

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

express()
  .use(cors({ origin: true, credentials: true }))
  .use(resolveSession)
  .use(resolveExport)
  .use(resolveImport)
  .use(resolveRun)
  .use(resolveQuery)
  .listen(4000, () => console.log('Server has started'));
