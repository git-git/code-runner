  CREATE TABLE "user" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar(100) NOT NULL,
  "login" varchar(100) NOT NULL,
  "password" varchar(100) NOT NULL,

  "created_at" timestamptz NOT NULL DEFAULT now(),
  "updated_at" timestamptz NOT NULL DEFAULT now()
);

CREATE TABLE "folder" (
    "id" SERIAL PRIMARY KEY,
    "name" varchar(100),
    "user_id" INTEGER NOT NULL REFERENCES "user" ("id"),
    "parent_id" INTEGER REFERENCES "folder" ("id"),

    "created_at" timestamptz NOT NULL DEFAULT now(),
    "updated_at" timestamptz NOT NULL DEFAULT now()
);

CREATE TABLE "project" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar(100) NOT NULL,
  "user_id" INTEGER NOT NULL REFERENCES "user" ("id"),
  "root_folder_id" INTEGER NOT NULL REFERENCES "folder" ("id"),

  "created_at" timestamptz NOT NULL DEFAULT now(),
  "updated_at" timestamptz NOT NULL DEFAULT now()
);

CREATE TABLE "file" (
    "id" SERIAL PRIMARY KEY,
    "name" varchar(100) NOT NULL,
    "user_id" INTEGER NOT NULL REFERENCES "user" ("id"),
    "parent_id" INTEGER REFERENCES "folder" ("id"),
    "content" TEXT NOT NULL DEFAULT '',

    "created_at" timestamptz NOT NULL DEFAULT now(),
    "updated_at" timestamptz NOT NULL DEFAULT now()
);
